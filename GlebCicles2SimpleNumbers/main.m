//
//  main.m
//  GlebCicles2SimpleNumbers
//
//  Created by Chaban Nikolay on 10/25/15.
//  Copyright © 2015 Gleb Cherkashyn. All rights reserved.
//

#import <Foundation/Foundation.h>


void simpleNumbersFinder(int startRange, int finishRange);

int main(int argc, const char * argv[]) {
    @autoreleasepool {
       
        simpleNumbersFinder(0, 300);
    }
    return 0;
}

void simpleNumbersFinder(int startRange, int finishRange) {
    while (startRange <= finishRange)
    {
        int currentNumber = startRange;
        int compositNumbersIdentifier = 0;
        
        for (int currentDenominator = 1; currentDenominator <= 100000; currentDenominator++)
        {
            
            if (currentDenominator > currentNumber)
            {
                break;
            }
            
            int reminder = currentNumber%currentDenominator;
            if (reminder == 0)
            {
                compositNumbersIdentifier++;
            }
            if (compositNumbersIdentifier >= 3) {
                break;
            }
            if ((currentNumber == currentDenominator)&&(compositNumbersIdentifier == 2)) {
                NSLog (@"Simple number %d", currentNumber );
            }
            
        }
        startRange++;
    }
}
